<!doctype html>
<html lang="en">
    @include('layouts._head')
    <body>
        @include('layouts._nav')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-12 col-lg-12 mr-sm-auto pt-3 px-4">
                <!-- main content -->
                @yield('content')
            </main>
        </div>
    </div>
    @include('layouts._footer')
  </body>
</html>
