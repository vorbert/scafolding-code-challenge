@extends('base')
@section('title', ' Product details')
@section('stylesheets')
@endsection

@section('content')
<table class="table">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Category</th>
            <th scope="col">Sub Category</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td> {{ $product->id }} </td>
            <td> {{ $product->mainCategoryName }} </td>
            <td> {{ $product->subCategoryName }} </td>
            <td> {{ $product->name }} </td>
            <td> {{ $product->price }} </td>
            <td> {{ $product->description  }} </td>
        </tr>
    </tbody>
</table>
@endsection