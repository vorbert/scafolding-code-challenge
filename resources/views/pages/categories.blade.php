@extends('base')
@section('title', ' Categories')
@section('stylesheets')
@endsection

@section('content')
<table class="table">
    <thead>
        <tr>
            <th scope="col">Categories</th>
            <th scope="col">Sub categories</th>
            <th scope="col">Products</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        @if(is_null($category->parent_id))
        <tr>
            <td> {{ $category->name }} </td>
            <td><button type="button" class="btn btn-secondary show-sub-cats" data-catid="{{ $category->id }}">Show sub categories</button></td>
            <td><a href="/categories/{{ strtolower($category->name) }}" target="_self"><button type="button" class="btn btn-dark">Show products</button></a></td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
@endsection