@extends('base')
@section('title', ' Products in {{ $categoryName }}')
@section('stylesheets')
@endsection

@section('content')
<table class="table">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Category</th>
            <th scope="col">Sub Category</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Details</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td> {{ $product->id }} </td>
            <td> {{ $product->mainCategoryName }} </td>
            <td> {{ $product->subCategoryName }} </td>
            <td> {{ $product->name }} </td>
            <td> {{ $product->price }} </td>
            <td><a href="/categories/{{ strtolower($categoryName) }}/{{ strtolower($product->subCategoryName) }}/{{ $product->id }}" target="_self"><button type="button" class="btn btn-dark">Show details</button></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $products->links() }}
@endsection