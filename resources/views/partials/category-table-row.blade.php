@foreach($subCategories as $subCategory)
<tr class="sub-category-rows-{{ $category->id }}">
    <td> {{ $category->name }} => {{ $subCategory->name }} </td>
    <td>&nbsp;</td>
    <td><a href="/categories/{{ strtolower($category->name) }}/{{ strtolower($subCategory->name) }} " target="_self"><button type="button" class="btn btn-dark show-products" data-catid="{{ $subCategory->id }}">Show products</button></td>
</tr>
@endforeach