    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Scaffolding code challenge">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="/favicon.ico">
        <title>{{ config('app.name', 'Scaffolding') }} - @yield('title')</title>
        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <link href="/css/bootstrap/bootstrap.min.css.map" rel="stylesheet">
        <!-- Custom styles for this template -->
        <!--<link href="/css/custom.css?{{ rand() }}" rel="stylesheet">-->
        @yield('stylesheets')
    </head>