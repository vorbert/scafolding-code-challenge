<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse">
    <ul class="navbar-nav px-3">
        <li class="nav-item">
            <a href="/" id="home" class="nav-link">Home</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="categories" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categories
            </a>
            <div class="dropdown-menu" aria-labelledby="categories">
                @foreach($categoryNames as $categoryName)
                <a class="dropdown-item" href="/categories/{{ strtolower($categoryName['name']) }}">{{ $categoryName['name'] }}</a>
                <ul>
                    @foreach($categoryName['subCategories'] as $subCategory)
                    <li>
                    <a class="dropdown-item" href="/categories/{{ strtolower($categoryName['name']) }}/{{ strtolower($subCategory['name']) }}">{{ $subCategory['name'] }}</a>
                    </li>
                    @endforeach
                </ul>
                @endforeach
            </div>
        </li>
        <li class="nav-item">
            <a href="/categories/all" id="products" class="nav-link">Products</a>
        </li>
    </ul>
    </div>
</nav>