    <footer>
    </footer>
    <script src="/js/jquery/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="/js/popper/popper.min.js"></script>
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    <script src="/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="/js/custom.js"></script>
    @yield('scripts')