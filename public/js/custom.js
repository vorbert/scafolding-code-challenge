var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$('.show-sub-cats').on('click', function(e) {
    var categoryId = $(this).data('catid');
    if($(this).attr('data-opened') != 1) {
        var button = $(this);
        $.ajax({
        url: '/get-subcategories/' + categoryId,
        data: {
            _token: CSRF_TOKEN
        },
        dataType: 'json',
        type: 'POST'
        }).done(function(data) {
            button.attr('data-opened', '1');
            button.text('Hide sub categories');
            $('.sub-category-rows-' + categoryId).remove();
            button.parent().parent().after(data.html);
        }).fail(function(data) {
            alert(data.msg);
        });
    } else {
        $(this).attr('data-opened', '0');
        $(this).text('Show sub categories');
        $('.sub-category-rows-' + categoryId).remove();
    }
});