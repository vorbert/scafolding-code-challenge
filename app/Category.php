<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{

    /**
     * getCategories function
     * This function returns all categories and sub categories from the database categories table
     *
     * @return iterable
     */
    public function getCategories() : iterable
    {
        $query = DB::table('categories')
                    ->orderBy('name', 'asc')
                    ->get();

        return $query;
    }


    /**
     * getCategoryById function
     * This function returns a category from the database by it's id
     *
     * @param [type] $id
     * @return object
     */
    public function getCategoryById(int $id) : object
    {
        $query = DB::table('categories')
                    ->where('id', '=', $id)
                    ->first();

        return $query;
    }


    /**
     * getSubCategoriesByCategoryId function
     * This function returns a sub categories's from the database by the category id
     *
     * @param [type] $id
     * @return void
     */
    public function getSubCategoriesByCategoryId(int $id) 
    {
        $query = DB::table('categories')
                    ->where('parent_id', '=', $id)
                    ->get();

        return $query;
    }


    /**
     * getCategoryByName function
     * This function returns a category from the database by the category name
     *
     * @param [type] $cagoryName
     * @return object
     */
    public function getCategoryByName(string $categoryName): object
    {
        $query = DB::table('categories')
                ->whereRaw('name = LOWER(?)', [$categoryName])
                ->first();
                
        return $query;
    }

}
