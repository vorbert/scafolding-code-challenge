<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{

    /**
     * getProductsByParentId function
     * This function returns data list collection from product table by parent_id in categories table which refers to the categor
     * e.g. phones is the main category which is a parent_id for apple subcategory which is a category_id for an apple phoe product
     *
     * @param [type] $categoryId
     * @return void
     */
    public function getProductsByParentId(int $categoryId)
    {
        $query = DB::table('products as p')
                    ->select('p.id as id', 'c.name as subCategoryName', 'p.name as name', 'p.price as price', 'c2.name as mainCategoryName')
                    ->join('categories as c', 'c.id', '=', 'p.category_id')
                    ->leftJoin('categories as c2', function ($j) {
                            $j->on('c2.id', '=', 'c.parent_id');
                    })
                    ->where(function ($q) use($categoryId) {
                        if($categoryId != 0) {
                            $q->where('c.parent_id', '=', $categoryId);
                        }
                    })
                    ->orderBy('p.id', 'asc')
                    ->paginate(5);
        return $query;
    }


    /**
     * getProductsBySubCategoryName function
     * This function returns data list collection from products table by subcategory name
     *
     * @param [type] $category
     * @param [type] $subCategoryName
     * @return void
     */
    public function getProductsBySubCategoryName(object $category, string $subCategoryName)
    {
        $query = DB::table('products as p')
                    ->selectRaw('p.id as id, c.name as subCategoryName, p.name as name, p.price as price, ? as mainCategoryName', [$category->name])
                    ->join('categories as c', 'c.id', '=', 'p.category_id')
                    ->whereRaw('c.name = LOWER(?)', [$subCategoryName])
                    ->whereRaw('c.parent_id = LOWER(?)', [$category->id])
                    ->orderBy('p.id', 'asc')
                    ->paginate(5);
        return $query;
    }


    /**
     * getProductDetails function
     * This function returns data row from product table by product id
     *
     * @param integer $productId
     * @param string $subCategoryName
     * @param string $categoryName
     * @return void
     */
    public function getProductDetails(int $productId, string $subCategoryName, string $categoryName)
    {
        $query = DB::table('products as p')
                    ->selectRaw('p.*, ? as subCategoryName, ? as mainCategoryName', [$subCategoryName, $categoryName])
                    ->where('id', '=', $productId)
                    ->first();
        return $query;
    }

}
