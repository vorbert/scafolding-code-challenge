<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Category;

class AppServiceProvider extends ServiceProvider
{
    private $categoryModel;

    public function __construct() 
    {
        $this->categoryModel = new Category();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories = $this->categoryModel->getCategories();
        $categoryNames = [];
        foreach($categories as $category) {
            if(is_null($category->parent_id)) {
                $categoryNames[$category->id] = [
                    'name' => $category->name,
                    'subCategories' => []
                ];
            }
        }
        foreach($categories as $category) {
            if(!is_null($category->parent_id)) {
                array_push($categoryNames[$category->parent_id]['subCategories'], 
                    ['name' => $category->name]
                );
            }
        }
        View::share('categoryNames', $categoryNames);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
