<?php

namespace App\Http\Controllers;

use Exception;
use App\Product;

class ProductController extends Controller
{

    private $categoryModel, $productsModel;


    /**
     * construct function
     */
    public function __construct() 
    {
        $this->productsModel = new Product();
    }


    /**
     * getProductDetails function
     * This function will redirect to product details view, with product data
     *
     * @param integer $productId
     * @return void
     */
    public function getProductDetails(string $categoryName, string $subCategoryName, int $productId)
    {
        try {
            $product = $this->productsModel->getProductDetails($productId, $subCategoryName, $categoryName);

            return view('pages.product-details', ['product' => $product]);
        } catch(Exception $e) {
            report($e);

            return false;
        }
    }
}
