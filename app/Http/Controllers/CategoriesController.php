<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Exception;

class CategoriesController extends Controller
{

    private $categoryModel, $productsModel;


    /**
     * construct function
     */
    public function __construct() 
    {
        $this->categoryModel = new Category();
        $this->productsModel = new Product();
    }


    /**
     * index function
     * This function renders the index page of the webapp with categories data
     *
     * @return void
     */
    public function index() 
    {
        try {
            $categories = $this->categoryModel->getCategories();

            return view('pages.categories', ['categories' => $categories]);
        } catch(Exception $e) {
            report($e);

            return false;
        }
    }


    /**
     * getSubCategoriesByCategoryId function
     * This function returns a table row html, filled with subcategory data
     *
     * @param [type] $id
     * @return void
     */
    public function getSubCategoriesByCategoryId(int $id) 
    {
        try {
            $category = $this->categoryModel->getCategoryById($id);
            $subCategories = $this->categoryModel->getSubCategoriesByCategoryId($id);
            $html = view("partials.category-table-row", ['subCategories' => $subCategories, 'category' => $category])->render();

            return response()->json(array('html' => $html), 200);
        } catch(Exception $e) {

            return response()->json(array('msg' => 'error'), 500);
        }
    }


    /**
     * getProductsByCategoryName function
     * This function return to product list view with data collected by category name
     *
     * @param [type] $categoryName
     * @return void
     */
    public function getProductsByCategoryName(string $categoryName)
    {
        try {
            if($categoryName == 'all') {
                $category = new Category();
                $category->id = 0;
                $category->name = $categoryName;
            } else {
                $category = $this->categoryModel->getCategoryByName($categoryName);
            }
            $products = $this->productsModel->getProductsByParentId($category->id);

            return view('pages.product-list', ['products' => $products, 'categoryName' => $category->name]);
        } catch(Exception $e) {
            report($e);

            return false;
        }
    }


    /**
     * getProductsBySubCategoryName function
     * This function returns product list view with data collected by subcategory name
     *
     * @param [type] $categoryName
     * @param [type] $subCategoryName
     * @return void
     */
    public function getProductsBySubCategoryName(string $categoryName, string $subCategoryName)
    {
        try {
            $category = $this->categoryModel->getCategoryByName($categoryName);
            $products = $this->productsModel->getProductsBySubCategoryName($category, $subCategoryName);

            return view('pages.product-list', ['products' => $products, 'categoryName' => $categoryName]);
        } catch(Exception $e) {
            report($e);

            return false;
        }
    }


    

}
